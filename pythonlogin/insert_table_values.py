# insert_table_values.py
# GH 2020.03.19 essai d'insertion

import connect_db

# Import le fichier "InsertOneTable" dans lequel il y a quelques classes et méthodes en rapport avec le sujet d'insertion dans UNE SEULE table
from INSERT import insert_one_table

objet_etre_connecte = connect_db.DatabaseTools()

# Une instance "insert_records" pour permettre l'utilisation des méthodes de la classe DBInsertOneTable
insert_records = insert_one_table.DbInsertOneTable()

#valeur_debile_mais_presque_aleatoire_a_inserer = "Hello toi"
#print(valeur_debile_mais_presque_aleatoire_a_inserer)
valeur_nom = " ATest "
valeur_description = " Joueur A va faire une balle haute." \
                     "Joueur B va alors se déplacer de manière à être positionné de manière correcte pour " \
                     "pouvoir frapper la balle soit au rebond soit en phase montante. Joueur B va/peut alors " \
                     "utiliser son bras libre (celui qui ne tient pas la raquette) afin de viser la balle et ainsi " \
                     "de la frapper au meilleur moment."
valeur_id = "4"

print(valeur_nom)
print()
print(valeur_description)

#insert_records.insert_one_record_one_table("INSERT IGNORE INTO t_genres (id_genre, intitule_genre) VALUES (null, %(values_insert)s)",valeur_debile_mais_presque_aleatoire_a_inserer)
#insert_records.insert_one_record_one_table("INSERT INGNORE INTO t_exercice (id_Exercice, Nom_Exercice) VALUES (null, %(values_imsert)s)", valeur_nom)

#***************************************************************************************************************************

insert_records.insert_one_record_one_table("INSERT IGNORE INTO t_exercice (id_Exercice, Visuel_Exercice, Nom_Exercice, Description_Exercice) VALUES (null, , %(values_insert)s,%(values_insert)s )", valeur_nom)#, valeur_description)

#***************************************************************************************************************************


# INSERT INTO `t_exercice` (`id_Exercice`, `Visuel_Exercice`, `Nom_Exercice`, `Description_Exercice`) VALUES (NULL, '', '', '')
objet_etre_connecte.close_connection()